'use strict';
const Net= require('net');
const Moment = require('moment');

module.exports.otaPing = (event, context, callback) => {

  var address = process.env.DESTINATION_ADDRESS; 
  var servicePort = process.env.DESTINATION_PORT;                                                                                                                          
  var datetime = Moment().format();
  var transID  = (new Date).getTime();                                                                                                                                            
  var time_start = (new Date).getTime();  
  console.log({
    "process": "unitedAilinesOtaPing",
    "stage": "starting",
    "time": datetime
  });
  connectSocket(datetime, transID, servicePort, address)
  .then((responseData)=>{
    var end_time=(new Date).getTime();
    const response = {
      "statusCode": 200,
      "body": JSON.stringify({
        "start_time": time_start,
        "end_time": end_time,
        "enlapsed_time": end_time - time_start
      }),
    };
    console.log(response)
    callback(null, response);
  })
  .catch((err)=>{
    console.log(err);
    const response = {
      "statusCode": 200,
      "body": JSON.stringify({
        "status": "error",
        "error": err
      }),
    };
    callback(null, response);
  });
};


const connectSocket = (datetime, transID, servicePort, address)=>{
  const client = new Net.Socket();
  return new Promise(function (resolve, reject){
    var dataToSend = process.env.XML_DATA_TO_SEND;
    client.connect(servicePort, address,()=>{
      console.log({
        "process": "OtaPing",
        "stage": "connected",
        "time": Moment().format()
      });
      client.write(dataToSend);
    });
    client.on('data', (data)=>{
      console.log({
        "process": "OtaPing",
        "stage": "data_reveived",
        "time": Moment().format()
      });
      client.destroy();
      resolve(data);
    });
    client.on('close', ()=>{
      console.log({
        "process": "OtaPing",
        "stage": "connection_closed",
        "time": Moment().format()
      });
    });
    client.on('error', (err)=>{
      console.log({
        "process": "OtaPing",
        "status": "error",
        "error": err,
        "time": Moment().format()
      });
      reject(err);
    });
  });
};
