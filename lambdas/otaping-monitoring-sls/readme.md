# United Airlines OTA ping

## Author

  Damian Olguin

## Description

This lambda function stablish a socket connection with given host and make a request, login all the steps with time stamps into cloudwatch logs, to be used to create Dashboards, detect anomaly and trigger alarms with cloudwatch.

## Resources

this lambda function has been made to be triggered by  `aws cloudwatch events`.


## Dependencies

This lambda function has dependency on:

* `net`: to stabish socket connections.
* `moment`: to handle time and time formats

## deployment

This lambda function has been deployed successfully using `serverless framework` but can be deployed with diferent tooks such as:

* cloud formation
* terraform
* aws sam
