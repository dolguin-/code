'use strict';
const AWS = require('aws-sdk');

AWS.config.apiVersions = {
  elasticbeanstalk: '2010-12-01'
}

module.exports.checkInstancesEB = (event, context, callback) => {
  var body=JSON.parse(event.body);
  getStackInfoEB(body.envName, body.region)
    .then((results) => {
      console.log('body', body);
      var readyToSwap = false;
      if(results && results.InstancesHealth) {
        if(results.InstancesHealth.Ok >= body.desiredInstanceCount) {
          readyToSwap = true;
        }
      }
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          status: results.Status,
          HealthStatus: results.HealthStatus,
          InstancesHealth: results.InstancesHealth,
          readyToSwap: readyToSwap
        })
      };
      callback(null, response);
    })
    .catch((err) => {
      console.error(err);
      const response = {
        statusCode: 500,
        body: JSON.stringify(err)
      };
      callback(null, response);
    });
}

const getStackInfoEB = (envName, region) => {
  const elasticbeanstalk = new AWS.ElasticBeanstalk({
    region: region
  });
  var params = {
    AttributeNames: [
       "All"
    ], 
    EnvironmentName: envName
   };
  console.log("envName:", envName);   
  console.log("params:", params);
  return new Promise(function (resolve, reject){
    elasticbeanstalk.describeEnvironmentHealth(params,(err, data) => {
      if (err) {
        console.log(err, err.stack);
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}