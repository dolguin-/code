# Pre swapping checks for elastic beanstalk stacks

## Author

  Damian Olguin

## Description

This lambda function will return a Json with information about the status of the given beanstalk stack.
Then you can use that information in another lambda to take the desition of swap or not swap the cname
in a blue/green deployment.

## Resources

this lambda function has been made to work behind of `aws api gateway` asociated with a `POST` method.

## Request

* Method: POST
* Path: /check/eb/instances
* Headers
  * x-api-key
* Body

```#!json
  {
    "region": "us-east-1",
    "envName": "stack-blue",
    "desiredInstanceCount": 2
  }
```

* body description
  * region: which aws region is located the beanstalk stack
  * envName: Name of the elastic beanstalk stack
  * desiredInstanceCount: mount of instances that should be online to be considered healty.

## Response

```#!JSON
{
    "status": "Ready",
    "HealthStatus": "Ok",
    "InstancesHealth": {
        "NoData": 0,
        "Unknown": 0,
        "Pending": 0,
        "Ok": 1,
        "Info": 0,
        "Warning": 0,
        "Degraded": 0,
        "Severe": 0
    },
    "readyToSwap": false
}
```

* response description
  * status: status of the beanstalk stack, if Ready it means that beanstalk is not applying any change on the stack
  * HealthStatus: this is the healty status regarding the elastic beanstalk metrics, it means that is passing the health checks and is not reporting errors to beanstalk.
  * InstancesHealth: This is related with the status of the instances in the stack, it's a list of status with the amount of instances on them.
  * readyToSwap: This field is true if the environment `status` is `Ready` and the `HealthStatus` is `ok` and the amount if instances in status `OK` is `equal or greater` that the given `desiredInstanceCount`.

## Dependencies

This lambda function only has dependency on `aws-sdk` which is included at the lambda environmen no there is no need to include node_modules folder on the deployment.

## deployment

This lambda function has been deployed successfully using `serverless framework` but can be deployed with diferent tooks such as:

* cloud formation
* terraform
* aws sam
