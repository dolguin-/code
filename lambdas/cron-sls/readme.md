# Invoke containers to run cronted workloads

## Author

  Damian Olguin

## Description

In order of replace cron jobs running on regular instances I made this lambda function to automate the process of spin up a new container of the given task definition on the given ECS cluster and run the work load based on cloudwatch logs events schedule which can be set using cron sintax or rate based.

## Resources

This lambda function has been made to work with `AWS Cloudwatch events` as a scheduler.

## Dependencies

This lambda function only has dependency on `aws-sdk` which is included at the lambda environmen no there is no need to include node_modules folder on the deployment.

## deployment

This lambda function has been deployed successfully using `serverless framework` but can be deployed with diferent tooks such as:

* cloud formation
* terraform
* aws sam
