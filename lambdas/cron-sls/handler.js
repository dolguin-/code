'use strict';

const AWS = require('aws-sdk');

const ecs = new AWS.ECS({
  region: process.env.REGION
});

module.exports.CronTaskRunner = (event, context, callback) => {

  const params = {
    cluster: process.env.CLUSTER_NAME,
    taskDefinition: process.env.TASK_NAME
  };

  ecs.runTask(params, function (err, data) {
    if (err) {
      const error_body={
        name: 'CronTaskRunner',
        action: 'run_ecs_task',
        task: params.taskDefinition,
        cluster: params.cluster,
        status: 'error',
        error: err,
        error_stack: err.stack,
        input: event
      };
      console.log(error_body);
      const response = {
        statusCode: 500,
        body: error_body
      }
      callback(null, response);
    } else {
      const body={
        name: 'CronTaskRunner',
        action: 'run_ecs_task',
        task: params.taskDefinition,
        cluster: params.cluster,
        status: 'successfull',
        data: data,
        input: event
      };
      console.log(body);
      const response = {
        statusCode: 200,
        body: body,
      };
      callback(null, response);
    }
  });
};
