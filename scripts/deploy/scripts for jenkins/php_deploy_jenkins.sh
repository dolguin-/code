#!/bin/bash -e


[[ -z ${source} ]] && source='development'

basepath=${WORKSPACE}
updateDate=$(date  '+%F %r (%Z)')

[[ ${ENVIRONMENT} == 'beta' \
    || ${ENVIRONMENT} == 'custom' ]] || exit 1;

#properties came from environment.properties file (iex: beta.properties live.properties)
source /data/settings/${JOB_NAME}/${ENVIRONMENT}/${ENVIRONMENT}.properties
source /data/settings/aws.${aws_account}.properties



function hipChatSend {
    curl -d "room_id=${hipchat_roomid}&from=DeploymentBot&message=$1&color=${hipchat_color}&notify=1" https://api.hipchat.com/v1/rooms/message?auth_token=${hipchat_token}&format=json
    echo ""
}


function checkSql {
    if [ -n $1 ]
    then
        #cargo la variable SCRIPT_NAME con el resultado de la query que busca el script_name que empiese con el valor contenido en la variable $1 que es recibida por parametro
        SCRIPT_NAME=$(mysql -A -u${dbuser} -p${dbpwd} -h${dbHost} ${dbname} -Bse "select script_name from Tracking_db_scripts where script_name like \"$1%\";")

        if [ -z "${SCRIPT_NAME}" ]
        then
            #si el contenido de SCRIPT_NAME es nulo salgo con 0 eso significa que se puede correr el script sin problemas
            echo 0;
        else
            #si el contenido de SCRIPT_NAME no es nulo salgo con 1 eso significa que NO se puede correr el script por que ya ha corrido
            echo 1;
        fi
    fi
    }

function sqlExec {
        #entro a la carpeta donde estan los scripts sql
        cd ${basepath}/db
        #saco el lastran de la tabla tracking_db_scripts de la base del environmnet que estamos trabajando (las credenciales vienen del archivo de conf ${env}.properties )
        LASTRUN=$(mysql -A -u${dbuser} -p${dbpwd} -h${dbHost} ${dbname} -Bse "select script_name from Tracking_db_scripts order by 1 DESC LIMIT 1;"|cut -d"_" -f1)
        echo -e "\n\nLastrun: ${LASTRUN}"

        #cargo la lista de todos """"""numeros"""""" de los scripts que corrieron en SKIP_LIST
        SKIP_LIST=$(mysql -A -u${dbuser} -p${dbpwd} -h${dbHost} ${dbname} -Bse "select script_name from Tracking_db_scripts order by 1;" | cut -d"_" -f1)

        # cargo en RUN_LIST la lista de los scripts que no se encuentras en ${SKIP_LIST} ordenada ascendentemente
        RUN_LIST=$(ls -1 |egrep -e "^[0-9]{6}_"|grep -Fv  "${SKIP_LIST}"|sort -n)
        echo -e "\n\nSQL scripts:"
        echo -e "------------"
        echo -e "${RUN_LIST}\n\n"

        #recorro la ${RUN_LIST}
        for scriptToRun in ${RUN_LIST}
        do
            #[Double-Check] Chequeo individualmente que el script en cuestrion no haya corrido antes
            if [ $(checkSql ${scriptToRun}) -ne 1 ]
            then
                echo -e "\tRunning:\t\t ${scriptToRun}";
                echo "mysql -u${dbuser} -p -h${dbHost} ${dbname} < \"${scriptToRun}\";"
                # corro el script y luego con [[ $? == 0 ]] chequeo el exit status del comando mysql para saver si dio algun error al ejecutar el script
                mysql -u${dbuser} -p${dbpwd} -h${dbHost} ${dbname} < "${scriptToRun}";
                [[ $? == 0 ]] && echo -e "\t-----------------------------------    ------------ [OK] " || echo -e "\tPlease check the ${scriptToRun} execution maybe it does not run as expected\n\n";
            else
                echo "status=Fail description=this script already ran"
            fi
        done
        #saco el lastran de la tabla tracking_db_scripts con el estado al finalizar la ejecucion
        LASTRUN=$(mysql -A -u${dbuser} -p${dbpwd} -h${dbHost} ${dbname} -Bse "select script_name from Tracking_db_scripts order by 1 DESC LIMIT 1;"|cut -d"_" -f1)
        #almaceno el valor de lastrun en lastrun.${ENVIRONMENT}
        echo ${LASTRUN} > /data/settings/${JOB_NAME}/${ENVIRONMENT}/lastrun.${ENVIRONMENT}
        echo -e "\n\n\t Last run: ${LASTRUN}\n"
    }

function configOverwrite {
        if [ -z "${OVERWRITE}" ]
        then
            echo -e "OVERWRIGE parameter has not been set"
            echo -e "please contact to CM Team\n\n"
            echo -e "Good Bye\n\n"
            exit 1;
        else
        	echo -e "\n\nApplying Config Overwrites\n"
            echo -e "${OVERWRITE} config_overwrite"
            cd ${exportdir}/$1
            echo "\n$(pwd)\n"
            ${OVERWRITE} config_overwrite;
            [[ $? == 0 ]] || exit 1;
            cp -rv config_overwrite/* .
            [[ $? == 0 ]] || exit 1;
            rm config_overwrite/ -rf
            [[ $? == 0 ]] || exit 1;
        fi
    }

function dirPrepare {
        revision=$1
        if [ -d ${exportdir}/${revision} ]
        then
            rm -rf ${exportdir}/${revision};
        fi
        cp "${basepath}" "${exportdir}/${revision}" -R
        echo -e "\n\n--------------"
        echo "upversion dir: ${exportdir}/${revision}"
        echo -e "--------------\n\n"
        
        if [ ${OVERWRITE_APPLY} == true ]; then
            configOverwrite ${revision}
        fi
        echo $(pwd)
        cd ${exportdir}/${revision}
        echo -e "\n\nRunning Composer.phar install -n --no-dev to install dependencies"
        composer.phar install -n --no-dev
        echo -e "\n\n Composer Done!\n\n"
        echo "Last Changes:" > lastchanges.txt
        git log -n 15 --oneline >> lastchanges.txt
        echo "Revision: ${revision}" > revision.txt
        echo "Update date: ${updateDate}" >> revision.txt
        if [ ! -z ${publicfolder} ]
        then
			update_date=$(date  '+%F %r (%Z)')
			echo -e "
            {
    			\"${PROJECT_NAME}\": {
        		    \"build\": \"${BUILD_NUMBER}\",
        		    \"revision\": \"${GIT_COMMIT}\",
        		    \"branch\": \"${source}\",
        		    \"build date\": \"${update_date}\",
        		    \"environment\": \"${ENVIRONMENT}\"
    			}
			}" > build.txt
			if [ ! -f build.txt ]; then
				echo -e "\n\n\t\tThe build json file wasn't write please check the permissions\n\n"
			else
            	echo -e "\n\nbuild json:\n"
				cat build.txt
			fi
            
            cp revision.txt ${publicfolder}
            cd ${publicfolder}
            ln -s revision.txt version.txt
        fi
        chmod ugo+rwx ${exportdir}/${revision} -R
        for i in ${files2delete};
        do
            echo "removing........  $i"; rm -rf  ${exportdir}/${revision}/$i;
        done
        cd "${basepath}";
    }

function codePackaging {
        revision=$1
        cd ${exportdir}
        echo -e "\n\nPackaging........  ${exportdir}/${revision}.${ENVIRONMENT}.tgz"
        if [ -w ${exportdir}/${revision}.${ENVIRONMENT}.tgz ]
        then
            echo "The revision ${revision} for ${ENVIRONMENT} already exists."
            echo "Do you want to overwrite it?: [true/FALSE]: ${overwrite}"
            if [[ (${overwrite} == 'FALSE' || ${overwrite} == 'false') ]]
            then
                echo -e "\n---------------"
                echo "Deploy Canceled"
                echo -e "---------------\n"
                echo -e "\n\nGood bye!\n\n"
                exit 1;
            else
                echo -e "\n---------------------"
                echo "  overwritting......... ${exportdir}/${revision}.${ENVIRONMENT}.tgz"
                echo -e "---------------------\n\n"
                rm -f ${exportdir}/${revision}.${ENVIRONMENT}.tgz
            fi
        fi
        tar czf ${exportdir}/${revision}.${ENVIRONMENT}.tgz ${revision}
        scrapCleanner ${revision}
    }

function codeUpload {
        #TODO add s3 bucket to properties file
        revision=$2;
        case $1 in
            "--s3")
                #check if ${bucket} is set
                if [ -z ${bucket} ]
                then
                    echo -e "ERROR:\n------\n"
                    echo -e "check if ${bucket} has been set at ${ENVIRONMENT}.properties\n\n if not fill it with the bucket path (ej: s3://deploys/\n)"
                    exit 1;
                else
                    echo -e "\n\n\t\t Uploading to S3: ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/versions/${revision}.tgz\n\n"
                    echo aws s3 cp ${exportdir}/${revision}.${ENVIRONMENT}.tgz ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/versions/${revision}.tgz
                    AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws s3 cp ${exportdir}/${revision}.${ENVIRONMENT}.tgz ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/versions/${revision}.tgz
                    (
                        echo -e "CurrentVersion:${revision}"
                        echo -e "PostDeploy:symlinks ${revision}, composer.phar install"
                    )>${exportdir}/info
                    echo aws s3 cp ${exportdir}/info ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/metadata/
                    AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws s3 cp ${exportdir}/info ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/metadata/
                    rm -f /tmp/info
                fi

            ;;
            "--server")
                if [ -z ${rsynchost} ]
                then
                    echo "Please fill rsynchost var at ${ENVIRONMENT}.properties"
                    exit 1;
                fi
                if [ -z ${rsyncuser} ]
                then
                    echo "Please fill rsyncuser var at ${ENVIRONMENT}.properties"
                    exit 1;
                fi
                echo "";
                echo "---------------"
                echo "   uploading"
                echo "---------------"
                scp  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${exportdir}/${revision}.${ENVIRONMENT}.tgz ${rsyncuser}@${rsynchost}:${rsynctargetdir}
                echo "---------------"
            ;;
        esac

    }

function codeDeploy {
        revision=$2;
        case $1 in
            "--s3")
                 #check if $bucket is set
                echo -e "\tHost Dicovering\n"
                if [ -n "${autoscalinggroup}" ]; then
                	HOSTS=$(aws-toolkit --ip ${autoscalinggroup});
                    echo -e "aws-toolkit --ip ${autoscalinggroup}\n\n"
                fi
                if [ -n "${extrahosts}" ]
                then
                    HOSTS+=" ${extrahosts}"
                fi
                if [ -z "${HOSTS}" ]
                then
                    echo -e "\n\n Host list is empty!"
                    echo -e "maybe the AWS is failing or not been configured yet!"
                    echo -e "please contact to CM Team\n\n"
                    echo -e "Good Bye\n\n"
                    exit 1;
                else
                	echo -e "\nHosts: ${HOSTS}\n\n"
                    for HOST in ${HOSTS}
                    do
                        echo -e "\t*** Flaging host ${HOST} to deploy!\n"
                        if [[ ${force} == false ]]
                        then
                            echo -e "touch /opt/ssdeploy/deploy.flag"|ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${HOST}
                        else
                            echo -e "echo \"F\" > /opt/ssdeploy/deploy.flag"|ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${HOST}
                        fi                    done
                    if [ -n ${extrascripts} ]
                    then
                        echo -e "${extrascripts} "|ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${HOST}
                    fi                    
                fi
            ;;
            "--server")
                if [ -z "${rsynchost}" ]
                then
                    echo "Please fill rsynchost var at ${ENVIRONMENT}.properties"
                    exit 1;
                fi
                if [ -z "${rsyncuser}" ]
                then
                    echo "Please fill rsyncuser var at ${ENVIRONMENT}.properties"
                    exit 1;
                fi
                echo -e "\n\n---------------"
                echo "   Rolling....."
                echo -e "---------------\n\n"
                echo -e "${symlink_path}/symlinks.sh ${linktarget} ${revision} | ssh -tt -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${rsynchost} \n\n"
                echo "${symlink_path}/symlinks.sh ${linktarget} ${revision}"| ssh -tt -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${rsynchost}
                echo -e "---------------\n"
            ;;
        esac
    }

function getLastRev {
    if [ ${deploy_to_s3} == true ]
    then

        AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws s3 cp ${bucket}/${ENVIRONMENT}/${PROJECT_NAME}/metadata/info /tmp/${PROJECT_NAME}.${ENVIRONMENT}.revision.txt > /dev/null 2>&1
        sleep 5;
    else
        scp  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ${rsyncuser}@${rsynchost}:${rsynctargetdir}../current/revision.txt /tmp/${PROJECT_NAME}.${ENVIRONMENT}.revision.txt > /dev/null 2>&1
    fi
    LAST_REV=$(cat /tmp/${PROJECT_NAME}.${ENVIRONMENT}.revision.txt |grep CurrentVersion|awk -F: '{print $2}')
    rm -f /tmp/${PROJECT_NAME}.${ENVIRONMENT}.revision.txt
    echo ${LAST_REV}
    }

function scrapCleanner {
        revision=$1
        rm -rf ${exportdir}/${revision}
    }

branch=$(echo ${GIT_BRANCH}|cut -d"/" -f2)
echo -e "current_Branch=${branch}"

echo -e "\nLast entries LOG:" && \
echo -e "-----------------" && \
git log --oneline -n30|egrep -v "^$"
REV_TO_PUBLISH=$(git log --oneline -n1|cut -d" " -f1)
REVS_AVAILABLE=$(git log --oneline -n30|sed "s/ .*$//"|tr "\n" " ")

if [[ ${DEPLOYABLE} == 'yes' ]]
then
    LASTR=$(getLastRev)
    echo $LASTR;
    DELTA=$(git log --oneline ${LASTR}..${REV_TO_PUBLISH})
    if [ -z "${DELTA}" ]
    then
        DELTA="\n      No Changes \n"
        if [[ ${force} == false ]]
        then
            echo -e "\n\n\n#####################################\n $DELTA \n#####################################\n\n";
            echo -e "Good Bye\n\n"
            exit 0;
        fi
    fi

    echo -en "\nRevision to Publish [${REV_TO_PUBLISH}]"
    if [ ${execute_sql} == true ]
    then
        sqlExec
    fi
    dirPrepare ${REV_TO_PUBLISH}
    codePackaging ${REV_TO_PUBLISH}

    if [ ${deploy_to_s3} == true ]
    then
        echo ' '
        codeUpload --s3 ${REV_TO_PUBLISH}
        codeDeploy --s3 ${REV_TO_PUBLISH}
    else
        
        codeUpload --server ${REV_TO_PUBLISH}
        codeDeploy --server ${REV_TO_PUBLISH}
    fi
    #hipChatSend "${PROJECT_NAME} ${ENVIRONMENT} Update - Publishing Revision: ${REV_TO_PUBLISH} from ${source}"

    echo -e "\n\n Enjoy the Party!!!!!.............\n\n"
    echo -e "Published Revision: ${REV_TO_PUBLISH}\n\nChangeLog: \n${DELTA} \n\nSQL scripts:\n ${RUN_LIST}\n"|mutt ${mailinglist} -c cm@mail.com -s "${PROJECT_NAME} - ${ENVIRONMENT} Release ChangeLog rev: ${REV_TO_PUBLISH}"
    echo -e "----------------------------------------------------------------------"
    echo -e "Published Revision: ${REV_TO_PUBLISH}\n\nChangeLog: \n${DELTA}"
    echo -e "----------------------------------------------------------------------"
    echo -e "SQL scripts:\n"
    for i in ${RUN_LIST}
    do
    	echo -e "$(basename $i)\n"
    done
    echo -e "----------------------------------------------------------------------\n\n"
    echo -e "\n\n\nGood Bye\n\n"
else
    echo -e "\nNOTICE: This environment (${ENVIRONMENT} --> ${ASG}) is NOT auto-deployable\n"
    echo -e "\n\nGood Bye\n\n"
fi
