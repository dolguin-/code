#/bin/bash
#
# author: Damian Olguin
#
# Description:
# This scrip update the environment variables used for assets versioning on applications that runs on 
# elastic beantalk.
#
# Environment variables needed in order to run this script:
#
# ENVIRONMENT
# JOB_NAME
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
#

source /data/settings/${JOB_NAME}/${ENVIRONMENT}/${ENVIRONMENT}.properties
source /data/settings/aws.${aws_account}.properties
OFFLIVE_ENV_NAMES=$(bash /data/scripts/ui/bluegreen/${ENVIRONMENT}/stacks.sh --deployable |ruby -r json -e "puts JSON.parse(ARGF.read)[\"Environment\"].map{|item| item['Name']}")

SPRINT_VERSION=$1

function setSprint {
    # parameteres:
    #    $1 Beanstalk Environment_name
    #    $2 SPRINT_VERSION
    #
    # return:
    #   Json with the instances ID's
    #
    #
    EB_NAME=$1
    SPRINT_VERSION=$2
    echo -e "\n\taws elasticbeanstalk update-environment --environment-name ${EB_NAME} --option-settings Namespace=aws:elasticbeanstalk:application:environment,OptionName=SPRINT_VERSION,Value=${SPRINT_VERSION}"
    AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk update-environment --environment-name ${EB_NAME} --option-settings Namespace=aws:elasticbeanstalk:application:environment,OptionName=SPRINT_VERSION,Value=${SPRINT_VERSION}

    }

for OFFLIVE_ENV_NAME in ${OFFLIVE_ENV_NAMES}; do
    ENV_SPRINT_VERSION=$(AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk describe-configuration-settings --application-name ${APPLICATION_NAME} --environment-name ${OFFLIVE_ENV_NAME}|ruby -r json -e "puts JSON.parse(ARGF.read)[\"ConfigurationSettings\"][0][\"OptionSettings\"].find { |opt| opt[\"OptionName\"]==\"SPRINT_VERSION\"}[\"Value\"]")

    if [ -z ${ENV_SPRINT_VERSION} ]; then
        echo -e "\n\n SPRINT_VERSION environment var is empty plase take alook at beanstalk.\n\n"
    fi

    if [ -z ${SPRINT_VERSION} ]; then
        if [ -z "${ENV_SPRINT_VERSION}" ]; then
            echo -e "\nNO SPRINT SET\n Please fill the SPRINT_VERSION field at the Eb environment name ${OFFLIVE_ENV_NAME} and try again\n"
            exit 1;
        else
            echo -e "\nNo Sprint version change for ${OFFLIVE_ENV_NAME}, using the value already set at EB: ${ENV_SPRINT_VERSION}\n"
        fi
    else
        if [ "x${SPRINT_VERSION}" != "x${ENV_SPRINT_VERSION}" ]; then
            echo -e "\n\n Updating ${OFFLIVE_ENV_NAME}: SPRINT_VERSION=${SPRINT_VERSION}\n\n"
            setSprint ${OFFLIVE_ENV_NAME} ${SPRINT_VERSION} ;
        else
            echo -e "The sprint version is up to date -for ${OFFLIVE_ENV_NAME}: SPRINT_VERSION=${ENV_SPRINT_VERSION}\n"
        fi
    fi
done

#WAITING FOR COMPLETE THE UPDATE
echo -e "\n\n\n Waiting for the environments to get the Status Ready"
for OFFLIVE_ENV_NAME in ${OFFLIVE_ENV_NAMES}; do
    STATUS=$(AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk describe-environments --environment-name ${OFFLIVE_ENV_NAME} |ruby -r json -e "puts JSON.parse(ARGF.read)['Environments'].map { |env| env['Status'] }")
    while [ "${STATUS}" != "Ready" ]
    do
        sleep 10;
        STATUS=$(AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk describe-environments --environment-name ${OFFLIVE_ENV_NAME} |ruby -r json -e "puts JSON.parse(ARGF.read)['Environments'].map { |env| env['Status'] }")
        echo "ENV=${OFFLIVE_ENV_NAME} Status=${STATUS}"
    done
done
#echo bash /data/scripts/ui/bluegreen/${ENVIRONMENT}/update-sprint-version.sh $(cat ${SPRINT_FILE})
