#!/bin/bash -x
#
# author: Damian Olguin
#
# Description:
# This scrip update the environment variables used for assets versioning on applications that runs on 
# elastic beantalk.
#
# Environment variables needed in order to run this script:
#
# ENVIRONMENT
# JOB_NAME
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
#

source /data/settings/${JOB_NAME}/${ENVIRONMENT}/${ENVIRONMENT}.properties
source /data/settings/aws.${aws_account}.properties
# DEFAULT_MIN, DEFAULT_MAX, PROJECT_URI, EbGreenStack, EbBlueStack are comming from the app property file

TEMPLATE_PATH=/tmp
TEMPLATE_FILE=setAutoscalingSize-template.json

if [ -f ${TEMPLATE_PATH}/${TEMPLATE_FILE} ]; then
    rm ${TEMPLATE_PATH}/${TEMPLATE_FILE}
fi


function buildTemplate {
    MIN_SIZE=$1
    MAX_SIZE=$2
    template="
    [
        {
            \"Namespace\": \"aws:autoscaling:asg\",
            \"OptionName\": \"MaxSize\",
            \"Value\": \"${MAX_SIZE}\"
        },
        {
            \"Namespace\": \"aws:autoscaling:asg\",
            \"OptionName\": \"MinSize\",
            \"Value\": \"${MIN_SIZE}\"
        }
    ]
"
    echo -e ${template} > ${TEMPLATE_PATH}/${TEMPLATE_FILE};
    #cat ${TEMPLATE_PATH}/${TEMPLATE_FILE};

    }

function setAutoscalingSize {
    MinSize=$1
    MaxSize=$2

    
    PUBLISH_ENV=$(curl --silent  ${PROJECT_URI}| ruby -r json -e "puts JSON.parse(ARGF.read)[\"env-code\"]")
    case ${PUBLISH_ENV} in
        "green")
            echo "Deploying into Blue Env: ${STACK}"
            buildTemplate ${MinSize} ${MaxSize}
            cat ${TEMPLATE_PATH}/${TEMPLATE_FILE}
            AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk update-environment --environment-name ${EbBlueStack} --option-settings file://${TEMPLATE_PATH}//${TEMPLATE_FILE}
        ;;
        "blue")
            echo "Deploying into Green Env: ${STACK}"
            buildTemplate ${MinSize} ${MaxSize}
            cat ${TEMPLATE_PATH}/${TEMPLATE_FILE}
            AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID} AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} aws elasticbeanstalk update-environment --environment-name ${EbGreenStack} --option-settings file://${TEMPLATE_PATH}//${TEMPLATE_FILE}
        ;;
    esac
}

function help {
    echo -e "\n usage $0 [--minimun|--standart|--custom]\n\n";
    if [ x${1} == xcustom ]; then
        echo -e "\n parameter missing please espesify all parameters needed for custom settings\n";
        echo -e "\n usage $0 --custom [min] [max]\n";
    fi
}
if [ $# -eq 0 ]; then
    help
    echo -e "\n\nError parameters missing \n"
    exit 1;
fi

case ${1} in
    "--minimun")
        setAutoscalingSize 1 1;
    ;;
    "--standart")
        setAutoscalingSize ${DEFAULT_MIN} ${DEFAULT_MAX};
    ;;
    "--custom")
        if [ $# -gt 2 ]; then
            setAutoscalingSize $2 $3;
        else
            help custom;
        fi
    ;;
    *)
        echo -e "\n usage $0 [--minimun|--standart|--custom]";
    ;;
esac

