module "demo_east2" {
  source             = "../../modules/vpc"
  vpcName            = "${var.environment}-${var.region}"
  environment        = "${var.environment}"
  region             = "${var.region}"
  zoneAlias          = "Ohio"
  cidr_block         = "${var.cidr_block}"
  az                 = ["us-east-2a", "us-east-2b"]
  public_count       = 2
  cidr_block_public  = "${var.cidr_block_public}"
  private_count      = 6
  cidr_block_private = "${var.cidr_block_private}"
  needsPeering       = false
  peer_owner_id      = ""
  peer_vpc_id        = ""
  dnsRoot            = "fakedomain.com"
}
