## variables
    variable "region" {
        # default = "us-east-2"
    }
    variable "environment" {
        # default = "demo"
    }
    variable "cidr_block" {
        # default="172.16.212.0/22"
    }
    variable "cidr_block_public" {
        type    = "list"
        # default = [ "172.16.212.0/25", "172.16.212.128/25" ]
    }
    variable "cidr_block_private" {
        type    = "list"
        # default = [ "172.16.213.0/25", "172.16.213.128/25", "172.16.214.0/25", "172.16.214.128/25", "172.16.215.0/25", "172.16.215.128/25" ]
    }