# DEMO VPC

This implementation of terraform scripts is an example of how to build a VPC wih an ECS cluster using this Modules:

* VPC
* ECS
* ASG
* SG
* SG_rules

## Inputs

| Name | Description | Type | Example | Required |
|------|-------------|:----:|:-----:|:-----:|
| cidr_block | CIDR block that will be used for the VPC network | string | `172.16.212.0/22` | yes |
| cidr_block_public | CIDR blocks that will be used by the public subnets, must be part of the VPC network | list | `[ "172.16.212.0/25", "172.16.212.128/25" ]` | yes |
| cidr_block_private | CIDR blocks that will be used by the private subnets, must be part of the VPC network | list | `[ "172.16.213.0/25", "172.16.213.128/25", "172.16.214.0/25", "172.16.214.128/25", "172.16.215.0/25", "172.16.215.128/25" ]` | yes |
| environment | Environment Name, will be use to name any component on the VPC | string | `demo` | yes |
| region | Region that Will be used to create the VPC and related resources | string | `us-east-2` | yes |

## Outputs

| Name | Description |
|------|-------------|
| ECS_CLUSTERS_ID | list of the ECS Clusters created |
| VPC INFO | display this information in map format: `vpc_id`, `nat_gateway_ips`, `private_subnet_ids`, `public_subnet_ids`.|

