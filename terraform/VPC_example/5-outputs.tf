## outputs
output "VPC INFO" {
    value = {
        vpc_id              = "${module.demo_east2.vpcId}",
        public_subnet_ids   = "${module.demo_east2.public_subnet_ids}",
        private_subnet_ids  = "${module.demo_east2.private_subnet_ids}",
        nat_gateway_ips     = "${module.demo_east2.natGwIp}"
    }
}

output "ECS_CLUSTERS_ID" {
    value = ["${module.ecs-cluster-beta.clusterId}"]
}