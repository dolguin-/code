## cluster Definitions
module "ecs-cluster-beta" {
  source      = "../../modules/ecs"
  clusterName = "${var.environment}"
}

## security groups
module "ecsEc2_sg" {
  source      = "../../modules/ec2/sg"
  name        = "ecsEc2-${var.environment}-sg"
  vpcId       = "${module.demo_east2.vpcId}"
  environment = "${var.environment}"
  project     = "containers"
}

# SG rules
module "ecsEc2_sg_ig_rule80" {
  source      = "../../modules/ec2/sg_rules"
  sgId        = "${module.ecsEc2_sg.sgId}"
  type        = "ingress"
  fromPort    = "0"
  toPort      = "0"
  protocol    = "tcp"
  cidrBlocks  = ["${var.cidr_block}"]
  description = "Allow all trafic from the local VPC network"
}

module "ecsEc2_sg_ig_rule22" {
  source      = "../../modules/ec2/sg_rules"
  sgId        = "${module.ecsEc2_sg.sgId}"
  type        = "ingress"
  fromPort    = "22"
  toPort      = "22"
  protocol    = "tcp"
  cidrBlocks  = ["0.0.0.0/0"]
  description = "allow ssh acces from anywhere"
}

module "ecsEc2_sg_eg_rule_all" {
  source      = "../../modules/ec2/sg_rules"
  sgId        = "${module.ecsEc2_sg.sgId}"
  type        = "egress"
  fromPort    = "0"                                #it means all
  toPort      = "0"
  protocol    = "-1"                               #it means all
  cidrBlocks  = ["0.0.0.0/0"]
  description = "allow egrass traffic to anywhere"
}

# ASG Definition

module "ecs-ec2instances" {
  source          = "../../modules/ec2/asg"
  environment     = "${var.environment}"
  vpcId           = "${module.demo_east2.vpcId}"
  create_asg      = true
  projectName     = "ecs-${var.environment}"
  asgName         = "ecs-${var.environment}"
  securityGroups  = ["${module.ecsEc2_sg.sgId}"]
  maxSize         = "0"
  minSize         = "0"
  desiredSize     = "0"
  instanceProfile = "arn:aws:iam::xxxxxxxxxxxx:instance-profile/ecsInstanceRole" ## update this with a valid ecs instance role
  ami             = "ami-b86a5ddd"
  instanceType    = "t2.small"
  keyName         = "BETA_key"

  userData = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${var.environment} >> /etc/ecs/ecs.config
EOF
}
