provider "aws" {
  profile = "beta"
  region  = "${var.region}"
  version = "~> 1.9"
}

terraform {
  backend "s3" {
    bucket         = "beta-terraform"
    key            = "examples/VPC/terraform.tfstate"
    region         = "us-east-1"
    profile        = "beta"
    dynamodb_table = "terraform-state-beta"
  }
}
