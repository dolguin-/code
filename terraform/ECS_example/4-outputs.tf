output "Microservice Api" {
  value = {
    host_based_url = "${module.microservice-api.url_host}"
    path_based_url = "${module.microservice-api.url_path}"
    ecs_cluster    = "beta"
    registry       = "${module.microservice_ecr.repositoryUrl}"
  }
}
