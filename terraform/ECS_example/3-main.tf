##  Modules 
# ECR REPO
module "microservice_ecr" {
  source      = "../../../modules/ecr"
  environment = "beta"
  projectName = "microservice-api"
}

# API
module "microservice-api" {
  source = "../modules/microservices/web"
  region = "us-east-1"

  vpcId           = "vpc-xxxxxxxx"              # update with a valid vpc
  ecsClusterName  = "beta"
  environment     = "beta"
  ecsTemplateFile = "${var.ecsApiTemplateFile}"
  imageVersion    = "${var.imageVersion}"
  projectName     = "${var.projectName}-api"
  rootZone        = "beta.id90.com"
  cname           = "${var.projectName}-api"
  port            = 3000
  protocol        = "tcp"
  health_check    = "/health"
  alb_name        = "beta-internal"

  #update with a valid loadbalancer and listeners arn
  alb_arn             = "arn:aws:elasticloadbalancing:us-east-1:xxxxxxxxxxxx:loadbalancer/app/beta-internal/3abd4343da2e12d0"
  alb_listener_arn80  = "arn:aws:elasticloadbalancing:us-east-1:xxxxxxxxxxxx:listener/app/beta-internal/3abd4343da2e12d0/790c99e38afc2dfc"
  alb_listener_arn443 = "arn:aws:elasticloadbalancing:us-east-1:xxxxxxxxxxxx:listener/app/beta-internal/3abd4343da2e12d0/828d182de6428999"
  alb_priority        = 1
  working_directory   = "/home/service"
  run_command         = "\"npm\", \"start\""
  ecr_repo            = "${module.microservice_ecr.repositoryUrl}"
  desired_count       = 1
  max_memory          = 256
  min_memory          = 128
  cpu_units           = 50
}
