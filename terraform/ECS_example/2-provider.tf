provider "aws" {
  profile = "beta"
  region  = "${var.region}"
  version = "~> 1.9"
}

provider "template" {
  version = "~> 1.0"
}

terraform {
  backend "s3" {
    bucket         = "beta-terraform"
    key            = "microservices/microservice-api/terraform.tfstate"
    region         = "us-east-1"
    profile        = "beta"
    dynamodb_table = "terraform-state-beta"
  }
}
