[
  {
    "cpu": ${cpu_units},
    "memoryReservation": ${min_memory},
    "memory": ${max_memory},
    "disableNetworking": false,
    "portMappings": [
        {
          "hostPort": 0,
          "containerPort": ${app_port_number},
          "protocol": "${app_port_protocol}"
        }
    ],
    "dnsServers": [
      "8.8.8.8",
      "8.8.4.4"
    ],
    "hostname": "${host_name}",
    "essential": true,
    "name": "${container_name}",
    "environment": [
      {
        "name": "NODE_ENV",
        "value": "beta"
      },
      {
        "name": "NODE_PATH",
        "value": "/home/service"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${logs_group}",
        "awslogs-region": "${logs_region}",
        "awslogs-stream-prefix": "${logs_stream_prefix}"
      }
    },
    "command": [ ${run_command} ],
    "workingDirectory": "${working_directory}",
    "image": "${ecr_repo}"
  }
]