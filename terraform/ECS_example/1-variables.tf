## variables
variable "ecsApiTemplateFile" {
  default = "web-task_definition_with_cloudwatch_logs.tpl"
}

variable "ecsWorkerTemplateFile" {
  default = "worker-task_definition_with_cloudwatch_logs.tpl"
}

variable "region" {
  default = "us-east-1"
}

variable "imageVersion" {
  default = "latest"
}

variable "projectName" {
  default = "microservice-api"
}
