## variables
    variable "vpcName" {}
    variable "zoneAlias" {}
    variable "region" {}
    variable "environment" {}

    # variable "vpnInstanceId" {}
    variable "needsPeering" { default = false }
    variable "peer_owner_id" {}
    variable "peer_vpc_id" {}
    variable "dnsRoot" {}
    variable "cidr_block" {}
    
    variable "public_count" {}
    variable "cidr_block_public" {
        type = "list"
    }

    variable "private_count" {}
    variable "cidr_block_private" {
        type = "list"
    }

    variable "az" {
        default = ["us-east-1a", "us-east-1b"]
    }
