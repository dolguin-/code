## modules
    module "public" {
        source      = "../subnet_count"
        environment = "${var.environment}"
        vpcId       = "${aws_vpc.vpc.id}"
        cidr_block  = "${var.cidr_block_public}"
        az          = "${var.az}"
        name        = "Pub"
        tier        = "public"
        count       = "${var.public_count}"
    }

    module "private" {
        source      = "../subnet_count"
        environment = "${var.environment}"
        vpcId       = "${aws_vpc.vpc.id}"
        cidr_block  = "${var.cidr_block_private}"
        az          = "${var.az}"
        name        = "Priv"
        tier        = "private"
        count       = "${var.private_count}"
    }
## output 

output "public_subnet_ids" {
    value = "${module.public.id}"
}

output "private_subnet_ids" {
    value = "${module.private.id}"
}