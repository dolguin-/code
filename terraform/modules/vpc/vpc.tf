## Resources

    resource "aws_vpc" "vpc" {
        cidr_block            = "${var.cidr_block}"
        enable_dns_support    = true
        enable_dns_hostnames  = true

        tags {
            Name = "${var.vpcName}"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

    resource "aws_vpc_dhcp_options" "vpcDhcpOptions" {
        domain_name          = "${var.region}.${var.dnsRoot}"
        domain_name_servers  = ["AmazonProvidedDNS"]

        tags {
            Name = "${var.vpcName}-${var.environment}"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

    resource "aws_vpc_dhcp_options_association" "vpcDhcpOptionsAssociation" {
        vpc_id          = "${aws_vpc.vpc.id}"
        dhcp_options_id = "${aws_vpc_dhcp_options.vpcDhcpOptions.id}"
    }

    module "igw" {
        source = "../igw"
        name   = "${var.vpcName}Igw"
        environment = "${var.environment}"
        vpcId = "${aws_vpc.vpc.id}"
    }

    resource "aws_eip" "eip1" {
        vpc = true
        tags {
            Name = "${var.vpcName}-${var.environment}Eip"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

    resource "aws_nat_gateway" "natgw1" {
        allocation_id = "${aws_eip.eip1.id}"
        subnet_id     = "${module.public.id[0]}"

        depends_on = ["module.igw"]
    }


    resource "aws_vpc_peering_connection" "VpcPeering" {
        count = "${var.needsPeering}"
        
        peer_owner_id = "${var.peer_owner_id}"
        peer_vpc_id   = "${var.peer_vpc_id}"
        vpc_id        = "${aws_vpc.vpc.id}"
        auto_accept   = true
        accepter {
            allow_remote_vpc_dns_resolution = true
        }
        requester {
            allow_remote_vpc_dns_resolution = true
        }
        tags {
            Name = "${aws_vpc.vpc.id}-to-${var.peer_vpc_id}"
            source = "${aws_vpc.vpc.id}"
            destination = "${var.peer_vpc_id}"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

## routing

    resource "aws_route_table" "public_RouteTable" {
        vpc_id = "${aws_vpc.vpc.id}"

        route {
            cidr_block = "0.0.0.0/0"
            gateway_id = "${module.igw.id}"
        }
        tags {
            Name = "public"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }
    resource "aws_route_table_association" "public" {
        count = "${var.public_count}"
        subnet_id = "${element(module.public.id, count.index)}"
        route_table_id = "${aws_route_table.public_RouteTable.id}"
    }
    resource "aws_route_table" "private_RouteTable" {
        vpc_id = "${aws_vpc.vpc.id}"

        route {
            cidr_block = "0.0.0.0/0"
            gateway_id = "${aws_nat_gateway.natgw1.id}"
        }
        tags {
            Name = "private"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }
    resource "aws_route_table_association" "private" {
        count = "${var.private_count}"
        subnet_id = "${element(module.private.id, count.index)}"
        route_table_id = "${aws_route_table.private_RouteTable.id}"
    }
    
## Outputs

output "vpcId" {
    value = "${aws_vpc.vpc.id}"
}

output "natGwIp" {
    value = "${aws_eip.eip1.public_ip}"
}