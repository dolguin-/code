
## Variables
    variable "name" {}
    variable "isInternal" {}
    variable "albSg" {
        # type = "list"
    }
    variable "subnetsId" {
        type = "list"
    }
    variable "environment" {}
    variable "project" {}
    # variable "logsBucket" {}
    variable "deleteProtection" {}
    variable "sslPolicy" {
        default = "ELBSecurityPolicy-2015-05"
    }
    variable "vpcId" {}
    variable "sslCertArn" {}

## Resources
    resource "aws_alb" "alb" {
        name            = "${var.name}"
        internal        = "${var.isInternal}"

        security_groups = [ "${var.albSg}" ]
        subnets         = [ "${var.subnetsId}" ]

        enable_deletion_protection = "${var.deleteProtection}"

        # access_logs {
        #     bucket = "${var.logsBucket}"
        #     prefix = "${var.name}-alb"
        # }

        tags {
            Environment = "${var.environment}"
            project     = "${var.project}"
            name        = "${var.name}"

        }
    }

    resource "aws_alb_target_group" "defaultTG" {
        name     = "${var.name}-default-80-tg"
        port     = 80
        protocol = "HTTP"
        vpc_id   = "${var.vpcId}"
        }

        resource "aws_alb_listener" "listener443" {
        load_balancer_arn = "${aws_alb.alb.arn}"
        port              = "443"
        protocol          = "HTTPS"
        ssl_policy        = "${var.sslPolicy}"
        certificate_arn   = "${var.sslCertArn}"

        default_action {
            target_group_arn = "${aws_alb_target_group.defaultTG.arn}"
            type             = "forward"
        }
        }
        resource "aws_alb_listener" "listener80" {
        load_balancer_arn = "${aws_alb.alb.arn}"
        port              = "80"
        protocol          = "HTTP"

        default_action {
            target_group_arn = "${aws_alb_target_group.defaultTG.arn}"
            type             = "forward"
        }
    }

## Outputs 

    output "albArn" {
        value = "${aws_alb.alb.arn}"
    }
    output "albName" {
        value = "${aws_alb.alb.name}"
    }
    output "albDnsName" {
        value = "${aws_alb.alb.dns_name}"
    }
    output "albCanonicalHostedZoneId" {
        value = "${aws_alb.alb.canonical_hosted_zone_id}"
    }
    output "albHostedZoneId" {
        value = "${aws_alb.alb.zone_id}"
    }
    output "albListener80Arn" {
        value = "${aws_alb_listener.listener80.arn}"
    }
    output "albListener443Arn" {
        value = "${aws_alb_listener.listener443.arn}"
    }