variable "name" {}
variable "vpcId" {}
variable "environment" {}
variable "project" {}

## Creates security group
resource "aws_security_group" "sg" {
  description = "${var.name} ${var.environment} security group"
  name_prefix = "${format("%s-%s-", var.name, var.environment)}"
  vpc_id      = "${var.vpcId}"

  tags {
    environment = "${var.environment}"
    project     = "${var.project}"
    managedBy   = "terraform"
    Name        = "${var.name}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

output "sgId" {
    value   = "${aws_security_group.sg.id}"
}
output "sgName" {
    value   = "${aws_security_group.sg.name}"
}