variable "sgId" {}
variable "type" {}
variable "fromPort" {}
variable "toPort" {}
variable "protocol" {}
variable "cidrBlocks" {
    type="list"
}
variable "description" {}

resource "aws_security_group_rule" "sg_rule" {
  type            = "${var.type}"
  from_port       = "${var.fromPort}"
  to_port         = "${var.toPort}"
  protocol        = "${var.protocol}"
  cidr_blocks     = "${var.cidrBlocks}"
  description     = "${var.description}"

  security_group_id = "${var.sgId}"
}