output "asg_arn" {
  value = "${aws_autoscaling_group.asg.arn}"
}
output "vpc_zones" {
  value= "${data.aws_subnet_ids.private.ids}"
}