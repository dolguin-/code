data "aws_subnet_ids" "private" {
  vpc_id = "${var.vpcId}"
  tags {
    Tier = "private"
  }
}