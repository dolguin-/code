variable "environment" {}
variable "projectName" {}
variable "vpcId" {}
variable "create_asg" {}
variable "asgName" {}
variable "maxSize" {}
variable "minSize" {}
variable "desiredSize" {}
variable "publicIp" { 
    default="false"
}
variable "instanceProfile" {
    # default=""
}
variable "ami" {}
variable "instanceType" {}
variable "keyName" {}
variable "healthCheckType" {
    default="EC2"
}
variable "cooldown" {
    default=300
}
variable "userData" {}
variable "root_vol_size" {
    default=30
}
variable "securityGroups" {
  type        = "list"
  description = "A list of associated security group IDs"
  default     = []
}
# variable "load_balancers" {}