# resource "aws_launch_configuration" "asgLc" {

#     name_prefix                 = "${var.asgName}LC-"
#     image_id                    = "${var.ami}"
#     associate_public_ip_address = "${var.publicIp}"
#     ebs_optimized               = "false"
#     enable_monitoring           = "${var.environment != "live" ? false : true }"
#     iam_instance_profile        = "${var.instanceProfile}"
#     instance_type               = "${var.instanceType}"
#     key_name                    = "${var.keyName}"
#     placement_tenancy           = "default"
#     security_groups             = ["${var.securityGroups}"]
#     user_data                   = "${var.userData}"

#     root_block_device {
#         # volume_size           = "${length(var.root_vol_size) > 0 ? var.root_vol_size : "30"}"
#         volume_type           = "gp2"
#         # delete_on_termination = true
#     }

#     lifecycle {
#         create_before_destroy = true
#     }
# }

# resource "aws_autoscaling_group" "asg" {
#     count                       = "${var.create_asg}" 

#     name_prefix                 = "${var.asgName}SG-"
#     launch_configuration        = "${aws_launch_configuration.asgLc.name}"
#     vpc_zone_identifier         = ["${data.aws_subnet_ids.private.ids}"]
#     max_size                    = "${var.maxSize}"
#     min_size                    = "${var.minSize}"
#     desired_capacity            = "${var.desiredSize}"

#     # launch_configuration        = ["${var.load_balancers}"]  #still having troubles with this one
#     health_check_grace_period   = 100
#     health_check_type           = "${var.healthCheckType}"

#     default_cooldown            = "${var.cooldown}"
#     force_delete                = true
# #   placement_group             = "${aws_placement_group.asgPg.id}"

#   tags = [
#         {
#             key                 = "Name"
#             value               = "${var.asgName}"
#             propagate_at_launch = true
#         },
#         {
#             key                 = "environment"
#             value               = "${var.environment}"
#             propagate_at_launch = true
#         },
#         {
#             key                 = "project"
#             value               = "${var.projectName}"
#             propagate_at_launch = true
#         },
#         {
#             key                 = "managedBy"
#             value               = "terraform"
#             propagate_at_launch = true
#         }
#     ]
#     timeouts {
#         delete = "15m"
#     }
# }