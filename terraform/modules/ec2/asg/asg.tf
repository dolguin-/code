resource "aws_launch_configuration" "asgLc" {

    name_prefix                 = "${var.asgName}LC-"
    image_id                    = "${var.ami}"
    associate_public_ip_address = "${var.publicIp}"
    ebs_optimized               = "false"
    enable_monitoring           = "${var.environment != "live" ? false : true }"
    iam_instance_profile        = "${var.instanceProfile}"
    instance_type               = "${var.instanceType}"
    key_name                    = "${var.keyName}"
    placement_tenancy           = "default"
    security_groups             = ["${var.securityGroups}"]
    # spot_price                  = "${var.spot_price}"
    user_data                   = "${var.userData}"

    root_block_device {
    #     volume_size           = "${length(var.root_vol_size) > 0 ? var.root_vol_size : "30"}"
        volume_type           = "gp2"
    }

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "asg" {
  vpc_zone_identifier        = ["${data.aws_subnet_ids.private.ids}"]
  name                      = "${var.asgName}"
  max_size                  = "${var.maxSize}"
  min_size                  = "${var.minSize}"
  health_check_grace_period = 100
  default_cooldown          = "${var.cooldown}"
  health_check_type         = "${var.healthCheckType}"
  desired_capacity          = "${var.desiredSize}"
  force_delete              = true
#   placement_group           = "${aws_placement_group.asgPg.id}"
  launch_configuration      = "${aws_launch_configuration.asgLc.name}"

  tags = [
    {
        key                 = "Name"
        value               = "${var.asgName}"
        propagate_at_launch = true
    },
    {
        key                 = "environment"
        value               = "${var.environment}"
        propagate_at_launch = true
    },
    {
        key                 = "project"
        value               = "${var.projectName}"
        propagate_at_launch = true
    },
    {
        key                 = "managedBy"
        value               = "terraform"
        propagate_at_launch = true
    }
  ]
  timeouts {
      delete = "15m"
    }
}