variable "clusterName" {}

resource "aws_ecs_cluster" "ecsCluster" {
  name = "${var.clusterName}"
}

output "clusterId" {
  value = "${aws_ecs_cluster.ecsCluster.id}"
}