variable "rootZone" {}
variable "type" {}
variable "name" {}
variable "aliasName" {}
variable "aliasZoneId" {}
variable "evalHealthCheck" {}

data "aws_route53_zone" "dnsZone" {
  name         = "${var.rootZone}."
}

resource "aws_route53_record" "DnsRecord" {
  zone_id = "${data.aws_route53_zone.dnsZone.zone_id}"
  name    = "${var.name}.${data.aws_route53_zone.dnsZone.name}"
  type    = "${var.type}"
  alias {
    name                   = "${var.aliasName}"
    zone_id                = "${var.aliasZoneId}"
    evaluate_target_health = "${var.evalHealthCheck}"
  }
}

output "fqdn" {
  value = "${aws_route53_record.DnsRecord.fqdn}"
}