variable "vpcId" {}
variable "cidr_block" {}
variable "az" {}
variable "name" {}
variable "tier" {}
variable "environment" {}

##resources
    resource "aws_subnet" "subnet" {
        vpc_id = "${var.vpcId}"

        cidr_block = "${var.cidr_block}"
        availability_zone = "${var.az}"

        tags {
            Name = "${var.name}",
            Tier = "${var.tier}"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

## outputs

    output "id" {
        value = ["${aws_subnet.subnet.id}"]
    }
    output "availavility_zone" {
        value = ["${aws_subnet.subnet.availavility_zone}"]
    }