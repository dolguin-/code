resource "aws_cloudwatch_log_group" "CloudWatchLogs" {
  name = "${var.projectName}-${var.environment}"
  retention_in_days = 30
  tags {
    Environment = "${var.environment}"
    Application = "${var.projectName}"
  }
}