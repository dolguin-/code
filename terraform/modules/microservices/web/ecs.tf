data "aws_ecs_cluster" "ecsCluster" {
  cluster_name = "${var.ecsClusterName}"
}