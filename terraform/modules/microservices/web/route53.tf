module "DnsRecord" {
  source = "bitbucket.org/dolguin-/code//terraform/modules/r53/alias"
  rootZone = "${var.rootZone}"
  type = "A"
  name = "${var.cname}.${var.rootZone}"
  aliasName = "${data.aws_alb.Alb.dns_name}"
  aliasZoneId = "${data.aws_alb.Alb.zone_id}"
  evalHealthCheck = false
}