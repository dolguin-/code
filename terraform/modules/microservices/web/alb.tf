data "aws_alb" "Alb" {
  arn  = "${var.alb_arn}"
  name = "${var.alb_name}"
}

data "aws_alb_listener" "listener80" {
  arn = "${var.alb_listener_arn80}"
}

data "aws_alb_listener" "listener443" {
  arn = "${var.alb_listener_arn443}"
}

resource "aws_alb_target_group" "AlbTg" {
  name     = "${var.projectName}-${var.environment}TG"
  port     = "${var.port}"
  protocol = "HTTP"
  vpc_id   = "${var.vpcId}"
  deregistration_delay = 60

  health_check {
      interval = 30
      path = "${var.health_check}"
      timeout = 5
      healthy_threshold = 2
      unhealthy_threshold = 4
      matcher = "200,401"
  }
}

resource "aws_alb_listener_rule" "patern_AlbListener80Rule" {
  listener_arn = "${data.aws_alb_listener.listener80.arn}"
  priority     = "${var.alb_priority}2"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.AlbTg.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/${var.projectName}-${var.environment}/*"]
  }
}

resource "aws_alb_listener_rule" "patern_AlbListener443Rule" {
  listener_arn = "${data.aws_alb_listener.listener443.arn}"
  priority     = "${var.alb_priority}2"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.AlbTg.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/${var.projectName}-${var.environment}/*"]
  }
}

resource "aws_alb_listener_rule" "host_AlbListener80Rule" {
  listener_arn = "${data.aws_alb_listener.listener80.arn}"
  priority     = "${var.alb_priority}3"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.AlbTg.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.cname}.${var.rootZone}"]
  }
}
resource "aws_alb_listener_rule" "host_AlbListener443Rule" {
  listener_arn = "${data.aws_alb_listener.listener443.arn}"
  priority     = "${var.alb_priority}3"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.AlbTg.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.cname}.${var.rootZone}"]
  }
}