# Project configuration 
variable "projectName" {}
variable "environment" {}
variable "working_directory" {}
variable "run_command" {}
variable "health_check" {}
variable "port" {}
variable "protocol" {}

# dns configuration
variable "cname" {}
variable "rootZone" {}

# networking configuration
variable "region" {}
variable "vpcId" {}

# cluster settings
variable "ecsClusterName" {}
variable "ecs_service_role" {
  default = "ecsServiceRole"
}

# Docker Image configuration
variable "ecr_repo" {}
variable "imageVersion" {}
# container settings
variable "desired_count" {}
variable "max_memory" {}
variable "min_memory" {}
variable "cpu_units" {}

# Application Loadbalancer configuration
variable "alb_name" {}
variable "alb_arn" {}
variable "alb_listener_arn80" {}
variable "alb_listener_arn443" {}
variable "alb_priority" {}

# logs settings
variable "log_retention" {
  default = 30
}