output "url_host" {
  value = "https://${module.DnsRecord.fqdn}/"
}

output "url_path" {
  value = "https://${data.aws_alb.Alb.dns_name}/${var.projectName}-${var.environment}/*"
}