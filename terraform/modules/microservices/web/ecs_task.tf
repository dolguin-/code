variable "ecsTemplateFile" {
    default = "/data/script/search-history-web/Test/templates/task-definitions/task_definition_with_cloudwatch_logs.tpl"
  # default = "/data/script/search-history-web/Test/task_definition.tpl"
}

data "template_file" "ecsTaskTemplate" {
  template = "${file("${var.ecsTemplateFile}")}"
  # template = "${file("${path.module}/templates/task-definitions/task_definition.tpl")}"
  # template = "${file("${path.module}/templates/task-definitions/task_definition_with_cloudwatch_logs.tpl")}"

  vars {
    min_memory        = "${var.min_memory}"
    max_memory        = "${var.max_memory}"
    app_port_number   = "${var.port}"
    app_port_protocol = "${var.protocol}"
    host_name         = "${var.projectName}-${var.environment}"
    container_name    = "${var.projectName}"
    working_directory = "${var.working_directory}"
    ecr_repo          = "${var.ecr_repo}:${var.imageVersion}"
    run_command       = "${var.run_command}"
    cpu_units         = "${var.cpu_units}"
    logs_group        = "${aws_cloudwatch_log_group.CloudWatchLogs.name}"
    logs_region       = "${var.region}"
    logs_stream_prefix= "${var.projectName}-${var.environment}"
    family            = "${var.projectName}-${var.environment}"
  }
}

resource "aws_ecs_task_definition" "ecsTask" {
  family                = "${var.projectName}-${var.environment}"
  container_definitions = "${data.template_file.ecsTaskTemplate.rendered}"
}

resource "aws_ecs_service" "ecsService" {
  name            = "${var.projectName}-${var.environment}"
  cluster         = "${data.aws_ecs_cluster.ecsCluster.id}"
  task_definition = "${aws_ecs_task_definition.ecsTask.arn}"
  deployment_minimum_healthy_percent = 100
  desired_count   = "${var.desired_count}"
  iam_role        = "${aws_iam_role.ecs_service.name}"

  load_balancer {
    target_group_arn = "${aws_alb_target_group.AlbTg.arn}"
    container_name   = "${var.projectName}"
    container_port   = "${var.port}"
  }
  depends_on = [
    "aws_iam_role.ecs_service",
    "aws_alb_listener_rule.patern_AlbListener443Rule",
    "aws_alb_listener_rule.patern_AlbListener80Rule",
    "aws_alb_listener_rule.host_AlbListener80Rule",
    "aws_alb_listener_rule.host_AlbListener443Rule",
  ]
}

# output "container_definition" {
#   value = "${data.template_file.ecsTaskTemplate.rendered}"
# }
