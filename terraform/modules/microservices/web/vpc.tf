data "aws_subnet_ids" "public" {
  vpc_id = "${var.vpcId}"
  tags {
    Tier = "public"
  }
}
data "aws_subnet_ids" "private" {
  vpc_id = "${var.vpcId}"
  tags {
    Tier = "private"
  }
}
data "aws_subnet" "public1" {
  id = "${data.aws_subnet_ids.public.ids[0]}"
}
# data "aws_subnet" "public2" {
#   id = "${data.aws_subnet_ids.public.ids[1]}"
# }
data "aws_subnet" "private1" {
  id = "${data.aws_subnet_ids.private.ids[0]}"
}
data "aws_subnet" "private2" {
  id = "${data.aws_subnet_ids.private.ids[1]}"
}