variable "name" {}
variable "vpcId" {}
variable "environment" {}

resource "aws_internet_gateway" "igw1" {
  vpc_id = "${var.vpcId}"
  tags {
    Name = "${var.name}"
    environment = "${var.environment}"
    managedBy = "terraform"
  }
}

output "id" {
    value = "${aws_internet_gateway.igw1.id}"
}