variable "projectName" {
}
variable "environment" {
}

resource "aws_ecr_repository" "ECR" {
  name = "${var.projectName}-${var.environment}"
}
output "repositoryUrl" {
  value = "${aws_ecr_repository.ECR.repository_url}"
}