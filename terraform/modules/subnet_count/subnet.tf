variable "vpcId" {}
variable "cidr_block" {
    type = "list"
}
variable "az" {
    type = "list"
}
variable "name" {}
variable "tier" {}
variable "environment" {}
variable "count" {}

##resources
    resource "aws_subnet" "subnet" {
        count = "${var.count}"
        vpc_id = "${var.vpcId}"

        cidr_block = "${element(var.cidr_block,count.index)}"
        availability_zone = "${element(var.az, count.index)}"

        tags {
            Name = "${var.name}${count.index}",
            Tier = "${var.tier}"
            environment = "${var.environment}"
            managedBy = "terraform"
        }
    }

## outputs

    output "id" {
        value = ["${aws_subnet.subnet.*.id}"]
    }
    output "availavility_zone" {
        value = ["${aws_subnet.subnet.availavility_zone}"]
    }